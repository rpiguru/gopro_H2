import copy
import json
import logging
import os
import platform
import threading
import time
import urllib2
import traceback
from flask import Flask, request, render_template


app = Flask(__name__)

cur_dir = os.path.dirname(os.path.realpath(__file__))


@app.route('/', methods=['GET'])
def root():
    return render_template('index.html')


@app.route('/get_schedule', methods=['GET'])
def get_schedule():
    with open(cur_dir + '/schedule.json') as json_file:
        data = json.load(json_file)
    return json.dumps(data)


@app.route('/update_schedule', methods=['GET'])
def update_schedule():
    day = request.args.get('day')
    enabled = request.args.get('enabled')
    start_time = request.args.get('start_time')
    end_time = request.args.get('end_time')
    interval = request.args.get('interval')

    json_file = open(cur_dir + '/schedule.json', 'r')
    data = json.load(json_file)
    json_file.close()

    json_file = open(cur_dir + '/schedule.json', 'w+')
    data[day]['enabled'] = enabled
    data[day]['start'] = start_time
    data[day]['stop'] = end_time
    data[day]['interval'] = interval
    json_file.write(json.dumps(data))
    json_file.close()

    return json.dumps('true')


if __name__ == '__main__':
    logging.basicConfig(level=10, filename=cur_dir+'/log.txt', format='%(asctime)s: %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.info("Script started")
    # logging.getLogger('werkzeug').setLevel(logging.ERROR)

    if platform.system() == 'Windows':
        app.run(port=8080, host="0.0.0.0", debug=False)
    else:
        app.run(port=80, host="0.0.0.0", debug=False)
