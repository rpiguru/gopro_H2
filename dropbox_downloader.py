import time

import dropbox


class DropBoxDownloader:

    client = None
    token = ''

    def __init__(self, token):
        self.token = token

    def connect(self):
        self.client = dropbox.client.DropboxClient(self.token)
        try:
            account_info = self.client.account_info()
            print 'Linked Account: ', account_info
            return True

        except dropbox.rest.ErrorResponse as e:
            print e
            return False

    def get_root_dir(self):
        return self.client.metadata('/')

    def download_file(self, metadata, dest_file_name=None):
        """
        Download file from dropbox
        :param metadata: starts with "/"
        :param dest_file_name: destination file name
        :return:
        """
        f, metadata = self.client.get_file_and_metadata(metadata)
        if dest_file_name is not None:
            out = open(dest_file_name, 'wb')
        else:
            out = open(metadata[1:], 'wb')

        out.write(f.read())
        out.close()
        print metadata

    def upload_file(self, dest_path, f_path):
        print "Uploading to DropBox, destinated path: ", dest_path, ', file_path: ', f_path
        f = open(f_path, 'rb')
        result = self.client.put_file(dest_path, f)
        print 'Result: ', result
        return result

    def create_folder(self, folder_name):
        try:
            self.client.file_create_folder(folder_name)
            return True
        except dropbox.rest.ErrorResponse as e:
            print e
            return False

if __name__ == '__main__':
    # f = open('config.xml', 'rb')
    # response = client.put_file('/magnum-opus.txt', f)
    # print 'uploaded: ', response
    s_time = time.time()
    d = DropBoxDownloader('SDx_t8AlZ8EAAAAAAAAlB5V5LGPengv1z4F-ibYT4TfYgLb9NvncARjDkJ-1Q1cB')

    d.connect()

    # print d.get_root_dir()

    for item in d.client.metadata('/Client2')['contents']:
        print "-" * 20
        for ii in item.items():
            print ii

    print "Elapsed: ", time.time() - s_time